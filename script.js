console.log("*** JS ARRAYS ***")

/*
Arrays - used to store multiple related values in a single variable
-declared using square brackets [] known as array literals

Elements - are values inside an array

index - 0 = offset > ground zero > where the event happens (reference with coordinates)
element 1 = index 0

Syntax: let/const arrayName = [elementA, elementB, elementC];
*/

let grades = [98, 94, 89, 90];
let computerBrands = ['Acer','Asus','Lenovo','Neo','Redfox','Gateway','Toshiba','Fujits'];
let mixedArr = [12,'Asus',null,undefined,{}];//not recommended
console.log(mixedArr);

//Reading
console.log(grades[0]);
console.log(computerBrands[3]);
console.log(grades[10])//this will result as undefined

let myTasks = [
    'bake sass',
    'drink html',
    'inhale css',
    'eat javascript',
];

console.log(myTasks);

//Reassigning value

myTasks[0]= 'hello world';

console.log(myTasks);

//getting the length of an array
console.log(computerBrands.length);

let lastIndex = myTasks.length-1;
console.log(lastIndex);

/*ARRAY METHODS
1. Mutator Methods - are functions that 'mutate'or change an array

push - adds an element in the end of the array and return its length
syntax: arrayName.push(elementA, elementB)
*/
let fruits = ['Apple', 'Orange', 'Kiwi','Dragon Fruit'];
console.log('Current array');
console.log(fruits);
console.log('Array after push()');

let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);

fruitsLength = fruits.push('Banana');
console.log(fruitsLength);


fruits.push('Guava','Avocado');
console.log(fruits);

//pop - removs the last element in an array and return the removed element

let removedFruit = fruits.pop();
console.log(fruits);
console.log(removedFruit);

//shift - removes the beginning element of an array and return the remove element

let firstElement = fruits.shift();
console.log(fruits);
console.log(firstElement);

//unshift - adds one or more element/s at the beginning of the an array
fruits.unshift('Lime','Banana');
console.log('Mutated array after unshift()');
console.log(fruits);

//splice - removes element from specified index and add new elements

//syntax: arrayName.splic(startingIndex, deleteCount, elements to be added)

fruits.splice(0,2, 'Cherry','Grapes');
console.log(fruits);

//sort - rearrange the elements in alphanumeric order
// fruits.sort();
// console.log('Mutated rray after sort()');
// console.log(fruits);

// //reverse - reverse the order of an array

// fruits.reverse();
// console.log('Mutated rray after sort()');
// console.log(fruits);




/*
2. Non Mutator Methods - are functions that do not modify or change the array
*/

let countries = ['US','PH','CAN','SG','TH','PH','FR','DE'];

//indexOf()
//-returns the frist index of the first mathching element found in an array
//i no match is found, it returns -1

let firstIndex = countries.indexOf('US');
console.log('Result of indexOf() method:' + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log('Result of indexOf() method:' +invalidCountry);

//lastIndexOf()
//-returns the last matching element found in an array
//syntax: arrayName.lastIndexOf(searchValue);
//arrayName.lastIndexof(searchvalue, fromIndex);

let lastIndex1 = countries.indexOf('PH');
console.log('Result of lastIndexOf() method:' + lastIndex1);

let lastIndexStart = countries.lastIndexOf('PH',7);
console.log('Result of lastIndexOf() method:' + lastIndexStart);

//slice - slices a portion of an array and return a new array
//syntax arrayName.slice()


let slicedArrayA = countries.slice(2);
console.log('Result from slice method A');
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2,4);
console.log('Result from slice method B');
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log('Result from slice method C');
console.log(slicedArrayC);

//toString

let stringArray = countries.toString();
console.log('Reult from to String()');
console.log(stringArray)

//concat - combines two or more arrays and return the combined result

let tasksArrayA = ['drink HTML','eat javascript'];
let tasksArrayB = ['inhale CSS','breathe sass'];
let tasksArrayC = ['get git','be node'];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log('Result from concat()');
console.log(tasks);

let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log('Result from concat()');
console.log(allTasks);

let combinedTasks = tasksArrayA.concat('smell express', 'throw react');
console.log('Result from concat()');
console.log(combinedTasks);

//join - returns an array as string seprated by specified separator

let users = ['John','Jane','Joe','Robert'];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));
console.log(users.join(' & '));




/*

3. Iteration methods - loops designed to perform repetitive tasks on arrays
//foreach
//syntax:
//arrayName.forEach(function(individualElement)){
    statement
}
*/

allTasks.forEach(function(task){
    console.log(task);
})

countries.forEach(function(country){
    console.log(country);
})

let filteredTasks = [];

allTasks.forEach(function(task){
    if(task.length > 10){
        filteredTasks.push(task);
    }
})
console.log('Result from forEach()');
console.log(filteredTasks);

//map - iterates on each element and returns new array with different
//values depending on the results of the function's operation

let numbers = [1,2,3,4,5];

let numberMap = numbers.map(function(number){
    return number*number;
});

console.log('Result form map()');
console.log(numberMap)
